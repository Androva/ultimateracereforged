﻿using System;
using System.Threading;

namespace UltimateRaceReforged
{
    class Program
    {
        static Scoreboard scoreboard = new Scoreboard();
        static Vehicle tesla = new eCar("Tesla Model-S", scoreboard);
        static Vehicle submarine = new Submarine("Virginia-Class Submarine", scoreboard);
        static Vehicle ktm = new Bike("KTM 450 Rally", scoreboard);
        static Vehicle robinson = new Helicopter("Robinson R22", scoreboard);
        static Thread[] contestantThreads = new Thread[scoreboard.numberOfVehicles];
        private static void HandleThreadStart(object index)
        {
            scoreboard.contestants[(int)index].startVehicle(contestantThreads[(int)index], scoreboard);
        }
        static void Main(string[] args)
        {
            for (int i = 0; i < scoreboard.numberOfVehicles; i++)
            {
                contestantThreads[i] = new Thread(HandleThreadStart);
                contestantThreads[i].Start(i);
            }
            while (scoreboard.position <= scoreboard.numberOfVehicles) {}
            Console.WriteLine("** RACE COMPLETED **");
            Console.WriteLine("** FINAL STANDINGS **");
            for (int vehicles = 0; vehicles < scoreboard.numberOfVehicles; vehicles++)
            {
                Console.WriteLine($"** {scoreboard.contestants[vehicles].name} finished in position\t\t{scoreboard.contestants[vehicles].finishPosition} **");
            }

        }
    }
}
