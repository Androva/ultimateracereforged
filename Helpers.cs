namespace UltimateRaceReforged
{
    public static class Helpers
    {
        public static T[] Push<T>(this T[] source, T value)
        {
            int originalLength = source.Length;
            int newLength = source.Length + 1;
            T[] newArray = new T[newLength];

            for (int i = 0; i < newLength; i++)
            {
                if (i != newLength - 1)
                    newArray[i] = source[i];
                else
                    newArray[i] = value;
            }

            return (newArray);
        }
    }
}