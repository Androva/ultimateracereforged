namespace UltimateRaceReforged
{
    public class Vehicle
    {
        public string name;
        public int contestantNumber;
        //In either kW (for electric vehicles) or litres (for all other vehicles)
        public double fuelCapacity;
        // kW/hour or litres/hour
        public int fuelUsagePerHour;
        public int avgSpeedKmh;
        public int avgSpeedKnots;
        public double timeToRefuelHrs;
        public double breakDownProbability;
        public int time = 0;
        public double totalFuel;
        public int distanceToWin;
        public int vehiclePosition = 0;
        internal string vehicleType;
        public bool raceComplete;
        public int finishPosition;
        private static Random random = new Random();

        public void startVehicle(Thread thread, Scoreboard scoreboard)
        {
            Console.WriteLine($"{this.name} starting...");
            int repairTime;

            repairTime = 0;
            while (true)
            {
                Thread.Sleep(1000);

                if (this.vehicleType == "sea")
                {
                    this.vehiclePosition += (int)this.avgSpeedKnots * 2;
                    Console.WriteLine($"{this.name} has travelled \t {this.vehiclePosition}km\n");
                }
                else
                {
                    this.vehiclePosition += (int)this.avgSpeedKmh;
                    Console.WriteLine($"{this.name} has travelled \t {this.vehiclePosition}km\n");


                    this.totalFuel -= this.fuelUsagePerHour;

                    if (this.totalFuel <= 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine($"** {this.name} is out of fuel! **\n** It will take {this.timeToRefuelHrs}hrs to refuel **\n");
                        Console.ResetColor();
                        Thread.Sleep((int)this.timeToRefuelHrs * 1000);
                        totalFuel = this.fuelCapacity;
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine($"** {this.name} has refuled and is back in the race! **\n");
                        Console.ResetColor();
                    }

                    if (random.NextDouble() < this.breakDownProbability)
                    {
                        repairTime = (int)(random.NextDouble() / random.NextDouble() * 1000) + 1000;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"** {this.name} has broken down! **\n** It will take {(repairTime / 1000).ToString()}hrs to repair **\n");
                        Console.ResetColor();
                        Thread.Sleep(repairTime);
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine($"** {this.name} is repaired and is back in the race! **\n");
                        Console.ResetColor();
                    }
                }

                if (this.vehiclePosition >= this.distanceToWin)
                {
                    this.raceComplete = true;
                    this.finishPosition = scoreboard.position;
                    if (scoreboard.position == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{this.name} HAS WON THE RACE!");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{this.name} has finished the race in position {scoreboard.position}!\n");
                        Console.ResetColor();

                    }

                    scoreboard.position++;
                    break;
                }
            }
        }
    }

    public class Helicopter : Vehicle
    {
        public Helicopter(string name, Scoreboard scoreboard)
        {
            this.name = name;
            this.fuelCapacity = 20;
            this.fuelUsagePerHour = 8;
            this.avgSpeedKmh = 180;
            this.timeToRefuelHrs = 3;
            this.breakDownProbability = 0.2;
            this.totalFuel = this.fuelCapacity;
            this.distanceToWin = 700;
            this.vehicleType = "air";
            this.contestantNumber = scoreboard.numberOfVehicles;
            scoreboard.numberOfVehicles++;
            if (scoreboard.contestants.Length < 1)
            {
                scoreboard.contestants = new Vehicle[1];
                scoreboard.contestants[0] = this;
            }
            else
                scoreboard.contestants = Helpers.Push(scoreboard.contestants, this);
        }
    }

    public class eCar : Vehicle
    {
        public eCar(string name, Scoreboard scoreboard)
        {
            this.name = name;
            this.fuelCapacity = 700;
            this.fuelUsagePerHour = 310;
            this.avgSpeedKmh = 120;
            this.timeToRefuelHrs = 1;
            this.breakDownProbability = 0.1;
            this.totalFuel = this.fuelCapacity;
            this.distanceToWin = 1200;
            this.vehicleType = "land";
            this.contestantNumber = scoreboard.numberOfVehicles;
            scoreboard.numberOfVehicles++;
            if (scoreboard.contestants.Length < 1)
            {
                scoreboard.contestants = new Vehicle[1];
                scoreboard.contestants[0] = this;
            }
            else
                scoreboard.contestants = Helpers.Push(scoreboard.contestants, this);
        }
    }

    public class Car : Vehicle
    {
        public Car(string name, Scoreboard scoreboard)
        {
            this.name = name;
            this.fuelCapacity = 50;
            this.fuelUsagePerHour = 10;
            this.avgSpeedKmh = 120;
            this.timeToRefuelHrs = 0.5;
            this.breakDownProbability = 0.1;
            this.totalFuel = this.fuelCapacity;
            this.distanceToWin = 1200;
            this.vehicleType = "land";
            this.contestantNumber = scoreboard.numberOfVehicles;
            scoreboard.numberOfVehicles++;
            if (scoreboard.contestants.Length < 1)
            {
                scoreboard.contestants = new Vehicle[1];
                scoreboard.contestants[0] = this;
            }
            else
                scoreboard.contestants = Helpers.Push(scoreboard.contestants, this);
        }
    }

    public class Bike : Vehicle
    {
        public Bike(string name, Scoreboard scoreboard)
        {
            this.name = name;
            this.fuelCapacity = 33.6;
            this.fuelUsagePerHour = 8;
            this.avgSpeedKmh = 100;
            this.timeToRefuelHrs = 0.5;
            this.breakDownProbability = 0.5;
            this.totalFuel = this.fuelCapacity;
            this.distanceToWin = 800;
            this.vehicleType = "land";
            this.contestantNumber = scoreboard.numberOfVehicles;
            scoreboard.numberOfVehicles++;
            if (scoreboard.contestants.Length < 1)
            {
                scoreboard.contestants = new Vehicle[1];
                scoreboard.contestants[0] = this;
            }
            else
                scoreboard.contestants = Helpers.Push(scoreboard.contestants, this);
        }
    }

    public class Submarine : Vehicle
    {
        public Submarine(string name, Scoreboard scoreboard)
        {
            this.name = name;
            this.fuelUsagePerHour = 310;
            this.avgSpeedKnots = 35;
            this.breakDownProbability = 0;
            this.distanceToWin = 1500;
            this.vehicleType = "sea";
            this.contestantNumber = scoreboard.numberOfVehicles;
            scoreboard.numberOfVehicles++;
            if (scoreboard.contestants.Length < 1)
            {
                scoreboard.contestants = new Vehicle[1];
                scoreboard.contestants[0] = this;
            }
            else
                scoreboard.contestants = Helpers.Push(scoreboard.contestants, this);
        }
    }
}